# Gulp Typescript Resolver

Rest in peace


## Usage

```
#!javascript
var ts = require('gulp-typescript');
var tsResolve = require('gulp-typescript-resolver');

gulp.src('lib/main.ts')
    .pipe(tsResolve()) // resolve all references & imports
    // You can add gulp-sourcemaps or a TypeScript preprocessor here
    .pipe(ts())
    .pipe(gulp.dest('release'));

```