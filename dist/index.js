/// <reference path='../typings/tsd.d.ts' />
var through = require('through2');
var gutil = require('gulp-util');
var PluginError = gutil.PluginError;
var fs = require('fs');
var path = require('path');
var Vinyl = require('vinyl');
var vinylfs = require('vinyl-fs');
// Consts
var PLUGIN_NAME = 'gulp-typescript-resolver';
var SEARCH_CONDITIONS = [
    'kjhkjh',
    'import [A-z]+ from \"[A-z./]+\"'
];
var resolvedPaths = [];
var resolvedFiles = [];
function resolve(file) {
    var fileContents = String(file.contents);
    var matches = (fileContents.match(new RegExp(SEARCH_CONDITIONS.join('|'), "g")) || []).map(function (substring) {
        var capturedPath = (/"(.*)"/g).exec(substring);
        if (capturedPath != null && capturedPath.length >= 1) {
            return capturedPath[1];
        }
    }).filter(function (element) {
        return element != null && resolvedPaths.indexOf(element) === -1;
    });
    resolvedPaths = resolvedPaths.concat(matches);
    resolvedFiles.push(file);
    matches.forEach(function (nextRelativePath) {
        var nextAbsolutepath = path.resolve(file.base, nextRelativePath) + '.ts';
        if (fs.existsSync(nextAbsolutepath)) {
            var nextFile = new Vinyl({
                cwd: file.cwd,
                base: path.dirname(nextAbsolutepath),
                path: nextAbsolutepath,
                contents: fs.readFileSync(nextAbsolutepath)
            });
            resolve(nextFile);
        }
        else {
            console.warn(PLUGIN_NAME + ': path doesn\'t exist: ' + nextAbsolutepath);
        }
    });
}
module.exports = function () {
    return through.obj(function (file, enc, callback /*: (err: boolean, chunk: gutil.File) => void*/) {
        var _this = this;
        if (file.isNull()) {
            resolvedFiles.push(null);
        }
        if (file.isStream()) {
            throw new PluginError(PLUGIN_NAME, 'Streams are not supported');
        }
        if (file.isBuffer()) {
            resolve(file);
        }
        resolvedFiles.forEach(function (file) {
            _this.push(file);
        });
        callback(false);
    });
};
