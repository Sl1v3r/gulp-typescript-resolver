'use strict';

var gulp = require('gulp');
var notify = require('gulp-notify');

var typeScript = require('gulp-typescript');

gulp.task('build', function () {
    return gulp.src([
        './typings/**/**.ts',
        './src/index.ts'
    ])
        .pipe(typeScript({
            target: 'es5',
            module: 'commonjs',
            noExternalResolve: true
        }))
        .on('error', notify.onError)
        .pipe(gulp.dest('./dist'))
});


gulp.task('watch', function () {
    gulp.watch('./src/**', ['build']);
});

gulp.task('default', ['build']);

gulp.task('serve', ['build', 'watch']);

