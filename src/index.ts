/// <reference path='../typings/tsd.d.ts' />

import through = require('through2');
import gutil = require('gulp-util');
import PluginError = gutil.PluginError;
import fs = require('fs');
import path = require('path');
import Vinyl = require('vinyl');
var vinylfs = require('vinyl-fs');


import ts = require('typescript');



// Consts
var PLUGIN_NAME = 'gulp-typescript-resolver';
var SEARCH_CONDITIONS: string[] = [
    'kjhkjh',
    'import [A-z]+ from \"[A-z./]+\"'
];

var resolvedPaths: string[] = [];
var resolvedFiles: gutil.File[] = [];

function resolve(file: gutil.File) {

    var fileContents: string = String(file.contents);

    var matches: string[] = (fileContents.match(new RegExp(SEARCH_CONDITIONS.join('|'), "g")) || [])
        .map((substring) => {
            var capturedPath = (/"(.*)"/g).exec(substring);
            if (capturedPath != null && capturedPath.length >= 1) {
                return capturedPath[1]
            }
        })
        .filter((element) => {
            return element != null && resolvedPaths.indexOf(element) === -1
        });

    resolvedPaths = resolvedPaths.concat(matches);
    resolvedFiles.push(file);

    matches.forEach((nextRelativePath: string) => {
        var nextAbsolutepath = path.resolve(file.base, nextRelativePath) + '.ts';
        if (fs.existsSync(nextAbsolutepath)) {

            var nextFile = new Vinyl({
                cwd: file.cwd,
                base: path.dirname(nextAbsolutepath),
                path: nextAbsolutepath,
                contents: fs.readFileSync(nextAbsolutepath)
            });

            resolve(nextFile)

        } else {
            console.warn(PLUGIN_NAME + ': path doesn\'t exist: ' + nextAbsolutepath)
        }
    });

}

module.exports = function() {

    return through.obj(function (file: gutil.File, enc: string, callback/*: (err: boolean, chunk: gutil.File) => void*/) {

        if (file.isNull()) {
            resolvedFiles.push(null)
        }
        if (file.isStream()) {
            throw new PluginError(PLUGIN_NAME, 'Streams are not supported');
        }
        if (file.isBuffer()) {
            resolve(file);
        }

        resolvedFiles.forEach((file) => {
           this.push(file);
        });

        callback(false);

    });

}
